package com.shop.profile.enumeration;

public enum ProfileStatus {

    ACTIVE, BLOCKED, DELETED
}
