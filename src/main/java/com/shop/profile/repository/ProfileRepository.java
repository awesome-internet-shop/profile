package com.shop.profile.repository;

import com.shop.profile.enumeration.ProfileStatus;
import com.shop.profile.model.enity.ProfileEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileRepository extends JpaRepository<ProfileEntity, Long> {

    @Modifying
    @Query("UPDATE ProfileEntity p SET p.status = :profileStatus WHERE p.id = :id")
    void updateProfileStatusById(@Param("id") Long id,
                                 @Param("profileStatus") ProfileStatus profileStatus);
}
