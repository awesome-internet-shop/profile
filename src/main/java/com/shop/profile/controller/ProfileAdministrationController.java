package com.shop.profile.controller;

import com.shop.profile.enumeration.ProfileStatus;
import com.shop.profile.model.dto.Profile;
import com.shop.profile.service.ProfileAdministrationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("admin/profiles")
public class ProfileAdministrationController {

    private final ProfileAdministrationService profileAdministrationService;

    @GetMapping
    public Page<Profile> getAllProfiles(@RequestParam int pageNumber,
                                        @RequestParam int pageSize) {
        return profileAdministrationService.getAllProfiles(pageNumber, pageSize);
    }

    @PatchMapping("{profileId}")
    public void updateProfileStatus(@PathVariable Long profileId,
                                    @RequestParam ProfileStatus status) {
        profileAdministrationService.updateProfileStatus(profileId, status);
    }

    @DeleteMapping("{profileId}")
    public void deleteProfile(@PathVariable Long profileId) {
        profileAdministrationService.deleteProfile(profileId);
    }
}
