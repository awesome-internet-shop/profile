package com.shop.profile.controller;

import com.shop.profile.model.dto.Profile;
import com.shop.profile.service.ProfileService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("profiles")
public class ProfileController {

    private final ProfileService profileService;

    @GetMapping("/{profileId}")
    public Profile getProfile(@PathVariable Long profileId) {
        return profileService.getProfile(profileId);
    }

    @PostMapping
    public void createProfile(@RequestBody Profile profile) {
        profileService.createProfile(profile);
    }
}
