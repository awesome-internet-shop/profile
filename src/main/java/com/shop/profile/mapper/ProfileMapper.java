package com.shop.profile.mapper;

import com.shop.profile.model.dto.Profile;
import com.shop.profile.model.enity.ProfileEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProfileMapper {

    Profile entityToDto(ProfileEntity entity);

    ProfileEntity dtoToEntity(Profile dto);
}
