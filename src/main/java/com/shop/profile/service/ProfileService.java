package com.shop.profile.service;

import com.shop.profile.exception.ProfileNotFoundException;
import com.shop.profile.mapper.ProfileMapper;
import com.shop.profile.model.dto.Profile;
import com.shop.profile.repository.ProfileRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProfileService {

    private final ProfileRepository profileRepository;
    private final ProfileMapper profileMapper;

    public void createProfile(Profile profile) {
        profileRepository.saveAndFlush(profileMapper.dtoToEntity(profile));
    }

    public Profile getProfile(Long profileId) {
        return profileRepository.findById(profileId)
                .map(profileMapper::entityToDto)
                .orElseThrow(ProfileNotFoundException::new);
    }
}
