package com.shop.profile.service;

import com.shop.profile.enumeration.ProfileStatus;
import com.shop.profile.mapper.ProfileMapper;
import com.shop.profile.model.dto.Profile;
import com.shop.profile.repository.ProfileRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProfileAdministrationService {

    private final ProfileRepository profileRepository;
    private final ProfileMapper profileMapper;

    public Page<Profile> getAllProfiles(int pageNumber, int pageSize) {
        return profileRepository.findAll(PageRequest.of(pageNumber, pageSize))
                .map(profileMapper::entityToDto);
    }

    public void updateProfileStatus(Long profileId, ProfileStatus newStatus) {
        profileRepository.updateProfileStatusById(profileId, newStatus);
    }

    public void deleteProfile(Long profileId) {
        profileRepository.deleteById(profileId);
    }
}
